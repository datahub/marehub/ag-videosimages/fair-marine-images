# Current version - v2.0.0
The state of the repository and documentation represents the current state-of-the-art of the iFDO standard. The 
current version as documented here is v2.0.0. The version of an iFDO can be specified by the 
`image-set-ifdo-version` field in the `image-set-header` part.

## Changelog

### v2.0.1 -> v2.1.0

- Added crosswalk from iFDO fields to PDS4 fields to JSON schema specification
- Added references to past schema definitions to documentation

### v2.0.0 -> v2.0.1

- relaxed jsonschema to also allow trimmed UUIDs (without hyphens)

### v1.2.0 -> v2.0.0
- Includes breaking changes to v1.x.y Sorry. All changes were made to move forwards with the iFDO concept towards a 
  robust metadata schema. Hence, there exists now a schema definition file for iFDOs, annotations and the provenance 
  format. This required to make the changes explained below in the other version changes of 
- It is now recommended to provide ifdo files in JSON format
- The field image-provenance has been added to document the agents, entities and activities that led to the creation 
  of the image set. This field is inspired by the definition of w3-prov
- The ambiguity of having two options for altitude / depth has been removed by dropping the image-depth field 
  entirely (you may use it if you like but the renamed field image-altitude-meters is required as well)
- Many fields that used to be strings are now objects to provide not only a value but also a link to external 
  resources, e.g. ```image-platform: {name: PFM-XYZ, uri: https://foobar/PFM-XYZ}```. Others fields that are affected: 
  image-sensor, image-license, image-context, image-project, image-event, image-pi
- The ``image-related-material`` field was added to reference information in external resources that relates to this 
  iFDO (e.g. cruise reports).
- The four fields image-set-[min,max]-[latitude,longitude]-degrees were added to define a bounding box within which all 
  images of the iFDO reside
- It is now required, that the iFDO ``image-set-header`` part includes one representative value for each of these 
  fields: ``image-datetime``, ``image-latitude``, ``image-longitude``, and ``image-altitude-meters``
- Some field names changes to either be properly spelled or include the unit name into the field name, e.g. for 
  ``image-altitude`` which is now called ``image-altitude-meters`` and ``image-area-square-meter``which is now called  
  ``image-area-square-meters`. 

### v1.1.0 -> v1.2.0
- Added core field `image-local-path`
- Added core field `image-handle`
- core fields are no longer strictly mandatory but most (as marked) are necessary to make the iFDO actually FAIR 

### v1.0.0 -> v1.1.0
- Added the required `image-set-ifdo-version` field as an `image-set-header`-only field of the iFDO core section
- Dropped the optional `image-pixel-per-millimeter` field from the iFDO capture section due to redundancy with `image-area-square-meter` field (you can keep using it if you like but tools might not understand it anymore)
- Renamed `image-resolution` in iFDO capture section to `image-pixel-magnitude` to avoid ambiguity
- Added the optional `image-datetime-format` field to specify non-standard date formats
- Added the `image-annotations:labels:created-at` field as an ISO8601 datetime of an annotation
- Corrected wrong data types in `image-camera-pose` subfields. 
- Modified the description texts (on using iFDO usage for video, image pixel coordinates, annotation confidence, )
- specified field data types consistently
- Added a roadmap of the iFDO development

