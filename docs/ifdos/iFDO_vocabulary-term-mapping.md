# Mapping iFDO fields to other standards
Fields of the [iFDO](iFDO-overview.md) image metadata standard are inspired by other metadata standards. iFDOs are 
designed to facilitate mapping between all relevant image metadata standards. Below, you can find a mapping between 
the iFDO fields and the corresponding field names (aka terms) in other metadata schemas. The iFDO JSON schema file 
implements this mapping in a machine-readable fashion.

## iFDO core fields
| [iFDOs](iFDO-overview.md) | [DarwinCore](https://dwc.tdwg.org/terms) | [Pangaea](https://wiki.pangaea.de/wiki/Data_submission) | [schema.org](https://schema.org/docs/schemas.html) | [OBIS](https://obis.org/manual/darwincore/) | [MEDIN/BODC](https://www.medin.org.uk/sites/medin/files/documents/MEDIN_Schema_Documentation3_1_brief.pdf) | [Audubon](https://ac.tdwg.org/termlist) | [SMarTaR-ID](https://doi.org/10.1371/journal.pone.0218904) | PDS4 |
| ------------------------- | ---------------------------------------- | ------------------------------------------------------- | ------------- | --------------- | - | - | - | - |
| image-set-name | datasetName | Dataset:Title | name | datasetName | Resource title |  |  | Product_Observational:Identification_Area:title |
| image-set-uuid | datasetID |  | identifier | datasetID | Unique resource identifier |  |  | Product_Bundle:Identification_Area:logical_identifier |
| image-set-handle |  | DOI (after publication) |  |  |  | ac:accessURI |  | Product_Observational:Identification_Area:Citation_Information:doi |
| image-datetime | eventDate | DATE/TIME (1599) | dateCreated | eventDate | Temporal reference  | ac:startTime | dateIdentified | Product_Observational:Observation_Area:Time_Coordinates |
| image-latitude | decimalLatitude | LATITUDE (1600) |  | decimalLatitude |  | dwc:decimalLatitude | decimalLatitude | Product_Observational:Observation_Area:Discipline_Area:Geometry:Vector_Planetocentric_Position_Base:latitude_position |
| image-longitude | decimalLongitude | LONGITUDE (1601) |  | decimalLongitude |  | dwc:decimalLongitude  | decimalLongitude | Product_Observational:Observation_Area:Discipline_Area:Geometry:Vector_Planetocentric_Position_Base:longitude_position |
| image-depth | verbatimDepth | DEPTH, water [m] (1619) |  | minimumDepthInMeters / maximumDepthInMeters | Vertical extent information | dwc:minimumDepthInMeters / dwc:maximumDepthInMeters | minimumDepthInMeters / maximumDepthInMeters | |
| image-altitude-meters | verbatimElevation | ALTITUDE [m] (4607) |  |  |  |  |  | Product_Observational:Observation_Area:Geometry:Geometry_Orbiter:Distances:Distance_Specific:spacecraft_target_center_distance |
| image-coordinate-reference-system | verbatimSRS |  |  | verbatimSRS | Spatial reference system |  |  | |
| image-coordinate-uncertainty-meters | coordinatePrecision | Coordinate uncertainty (170981) |  | coordinateUncertaintyInMeters |  | dwc:pointRadiusSpatialFit |  | |
| image-project |  | Dataset:Project | isPartOf |  |  |  |  | Product_Observational:Observation_Area:Investigation_Area:name |
| image-context |  |  |  |  |  |  |  | |
| image-event |  | Event |  |  |  |  |  | Product_Observational:Observation_Area:Mission_Area:Mission_Information:mission_phase_identifier |
| image-platform |  | Event:Platform |  |  |  |  |  | |
| image-sensor |  | Event:Sensor |  |  |  | ac:captureDevice? |  | |
| image-uuid | eventID |  | identifier | eventID |  |  |  | Product_Observational:Identification_Area:logical_identifier |
| image-hash-sha256 |  |  |  |  |  | ac:hashValue |  | Product_Observational:File_Area_Observational:File:md5_checksum |
| image-filename |  | File name (25541) | name |  |  |  |  | Product_Observational:File_Area_Observational:File:file_name |
| image-pi |  | Dataset:PI |  |  | Originator |  |  | Product_Observational:Identification_Area:Citation_Information:editor_list |
| image-creators | rightsHolder | Dataset:Authors | creator | rightsHolder | Responsible party | dc::creator |  | Product_Observational:Identification_Area:Citation_Information:author_list |
| image-license | license | Dataset:License | license | license | Limitations on public access/ Conditions applying for access and use |  |  | |
| image-copyright |  |  |  |  |  |  |  | |
| image-abstract |  | Dataset:Abstract |  |  | Resource abstract |  |  | Product_Observational:Identification_Area:Citation_Information:description |

## iFDO capture fields
| [iFDOs](iFDO-overview.md) | [DarwinCore](https://dwc.tdwg.org/terms) | [Pangaea](https://wiki.pangaea.de/wiki/Data_submission) | [schema.org](https://schema.org/docs/schemas.html) | [OBIS](https://obis.org/manual/darwincore/) | [MEDIN/BODC](https://www.medin.org.uk/sites/medin/files/documents/MEDIN_Schema_Documentation3_1_brief.pdf) | [Audubon](https://ac.tdwg.org/termlist) | [SMarTaR-ID](https://doi.org/10.1371/journal.pone.0218904) | PDS4 |
| - | - | - | - | - | - | - | - | - |
| image-acquisition | type |  | ImageObject / VideoObject | type | Resource type / Data format | dc:format? |  | Product_Observational:Observation_Area:Investigation_Area:type |
| image-quality |  |  |  |  |  |  |  |  |
| image-deployment |  |  |  |  |  |  |  |  |
| image-navigation |  |  |  |  |  |  |  |  |
| image-scale-reference |  |  |  |  |  |  |  |  |
| image-illumination |  |  |  |  |  |  |  |  |
| image-resolution |  |  |  |  |  |  |  |  |
| image-marine-zone |  |  |  |  |  |  |  |  |
| image-spectral-resolution |  |  |  |  |  |  |  |  |
| image-capture-mode |  |  |  |  |  |  |  |  |
| image-area-square-meter |  |  |  |  |  |  |  |  |
| image-pixel-per-millimeter |  | Image resolution (172673) |  |  | Spatial resolution |  |  |  |
| image-meters-above-ground |  | HEIGHT above ground [m] (56349) |  |  | Distance  |  |  |  |
| image-acquisition-settings |  |  |  |  | Lineage(?) |  |  | Product_Observational:Observation_Area:Discipline_Area:Imaging |
| image-camera-yaw-degrees |  |  |  |  |  |  |  |  |
| image-camera-pitch-degrees |  |  |  |  |  |  |  |  |
| image-camera-roll-degrees |  |  |  |  |  |  |  |  |
| image-camera-pose |  |  |  |  |  |  |  |  |
| image-camera-housing-viewport |  |  |  |  |  |  |  |  |
| image-camera-flatport-parameters |  |  |  |  |  |  |  |  |
| image-camera-domeport-parameters |  |  |  |  |  |  |  |  |
| image-camera-calibration-model |  |  |  |  |  |  |  |  |
| image-camera-photometric-calibration |  |  |  |  |  |  |  |  |
| image-objective |  |  |  |  |  |  |  |  |
| image-target-environment |  |  |  |  |  | dwc:locationRemarks? |  |  |
| image-target-timescale |  |  |  |  |  |  |  |  |
| image-spatial-constraints | footprintWKT? |  |  |  |  | dwc:footprintWKT? | locality? |  |
| image-temporal-constraints |  |  |  |  |  | dcterms:temporal? |  |  |
| image-reference-calibration |  |  |  |  |  |  |  |  |
| image-time-synchronization |  |  |  |  |  |  |  |  |
| image-item-identification-scheme |  |  |  |  |  |  |  |  |
| image-curation-protocol |  |  |  |  |  |  |  |  |

## iFDO content fields
| [iFDOs](iFDO-overview.md) | [DarwinCore](https://dwc.tdwg.org/terms) | [Pangaea](https://wiki.pangaea.de/wiki/Data_submission) | [schema.org](https://schema.org/docs/schemas.html) | [OBIS](https://obis.org/manual/darwincore/) | [MEDIN/BODC](https://www.medin.org.uk/sites/medin/files/documents/MEDIN_Schema_Documentation3_1_brief.pdf) | [Audubon](https://ac.tdwg.org/termlist) | [SMarTaR-ID](https://doi.org/10.1371/journal.pone.0218904) | 
| - | - | - | - | - | - | - | - |
| image-entropy |  |  |  |  |  |  |  |
| image-particle-count |  |  |  |  |  |  |  |
| image-average-color |  |  |  |  |  |  |  |
| image-mpeg7-... |  |  |  |  |  |  |  |
| image-annotations |  |  |  |  |  | ac:subjectOrientation? | |
| image-annotation-labels | taxonID? |  |  |  |  | dwc:scientificName? | scientificName / scientificNameID |
| image-annotation-creators | identifiedBy? |  |  |  |  | dwc:identifiedBy? | identifiedBy |


## Disclaimer
Vocabularies, repositories or other sources that do not enforce structured metadata or data (like Zenodo or DRYAD) 
are not considered here as they do not lead to FAIRness of data. Similarly, all projects, workflows, etc. that rely 
on these sources are omitted as well.
