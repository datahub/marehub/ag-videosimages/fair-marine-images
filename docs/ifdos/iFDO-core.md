# iFDO core
The iFDO core is one section of the complete [iFDO file](iFDO-overview.md). See its description to learn about its 
file formats, parts and sections.

## Header information in the `image-set-header` part
These three `image-set-header` values have to exist and cannot be superseded by values of the `image-set-items`. 
Fields from the `image-set-items` part (below) may exist in the header as default values for the entire image set.

| Field                                                                        | Comment | Required for FAIRness  |
|------------------------------------------------------------------------------| ------- |------------------------|
| [image-set-name](../schemas/ifdo/v2.1.0/html/image-set-name)                 | A unique name for the image set, should include `<project>, <event>, <sensor>` and purpose | ✓                      |
| [image-set-uuid](../schemas/ifdo/v2.1.0/html/image-set-uuid)                 | A UUID for the entire image set | ✓                      |
| [image-set-handle](../schemas/ifdo/v2.1.0/html/image-set-handle)             | A Handle URL to point to the landing page of the data set | ✓                      |
| [image-set-ifdo-version](../schemas/ifdo/v2.1.0/html/image-set-ifdo-version) | The semantic version information of the iFDO standard used | ✓                      |

In JSON, the entire `image-set-header` part is one dictionary.

## Image item information in the `image-set-items` part

| Field                                                                                                  | Comment                                                                                                                                                                          | Required for FAIRness |
|--------------------------------------------------------------------------------------------------------|----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|--------------------|
| [image-datetime](../schemas/ifdo/v2.1.0/html/image-datetime)                                           | The fully-qualified ISO8601 UTC time of image acquisition (or start time of a video). E.g.: %Y-%m-%d %H:%M:%S.%f (in Python). Other formats possible with using `image-datetime-format` | ✓ |
| [image-latitude](../schemas/ifdo/v2.1.0/html/image-latitude)                                           | Y-coordinate of the camera center in decimal degrees: D.DDDDDDD (use at least seven significant digits that is ca. 1cm resolution)                                               | ✓ |
| [image-longitude](../schemas/ifdo/v2.1.0/html/image-longitude)                                         | X-coordinate of the camera center in decimal degrees: D.DDDDDDD (use at least seven significant digits that is ca. 1cm resolution)                                               | ✓ |
| [image-altitude-meters](../schemas/ifdo/v2.1.0/html/image-altitude-meters)                                           | Z-coordinate of camera center in meters. When camera is above sea level, then it has positive values. When the camera is below sea level it has negative values.                 | ✓ |
| [image-coordinate-reference-system](../schemas/ifdo/v2.1.0/html/image-coordinate-reference-system)     | The coordinate reference system, e.g. EPSG:4326                                                                                                                                  |  |
| [image-coordinate-uncertainty-meters](../schemas/ifdo/v2.1.0/html/image-coordinate-uncertainty-meters) | The average/static uncertainty of coordinates in this dataset, given in meters. Computed e.g. as the standard deviation of coordinate corrections during smoothing / splining.   |  |
| [image-context](../schemas/ifdo/v2.1.0/html/image-context)                                             | The high-level "umbrella" project                                                                                                                                                | ✓ |
| [image-project](../schemas/ifdo/v2.1.0/html/image-project)                                             | The lower-level / specific expedition or cruise or experiment or ...                                                                                                             | ✓ |
| [image-event](../schemas/ifdo/v2.1.0/html/image-event)                                                 | One event of a project or expedition or cruise or experiment or ...                                                                                                              | ✓ |
| [image-platform](../schemas/ifdo/v2.1.0/html/image-platform)                                           | Platform URN or Equipment Git ID or Handle URL                                                                                                                                   | ✓ |
| [image-sensor](../schemas/ifdo/v2.1.0/html/image-sensor)                                               | Sensor URN or Equipment Git ID or Handle URL                                                                                                                                     | ✓ |
| [image-uuid](../schemas/ifdo/v2.1.0/html/image-uuid)                                                   | UUID for the image file (still or moving)                                                                                                                                        | ✓ |
| [image-hash-sha256](../schemas/ifdo/v2.1.0/html/image-hash-sha256)                                     | An SHA256 hash to represent the whole file (including UUID in metadata!) to verify integrity on disk                                                                             | ✓ |
| [image-pi](../schemas/ifdo/v2.1.0/html/image-pi)                                                       | Information to identify the principal investigator. See details below                                                                                                            | ✓ |
| [image-creators](../schemas/ifdo/v2.1.0/html/image-creators)                                           | A list containing dicts for all creators containing: {orcid:..., name:...}                                                                                                       | ✓ |
| [image-license](../schemas/ifdo/v2.1.0/html/image-license)                                             | License to use the data (should be FAIR, e.g. CC-BY or CC-0)                                                                                                                     | ✓ |
| [image-copyright](../schemas/ifdo/v2.1.0/html/image-copyright)                                         | Copyright sentence / contact person or office                                                                                                                                    | ✓ |
| [image-abstract](../schemas/ifdo/v2.1.0/html/image-abstract)                                           | 500-2000 characters describing what, when, where, why and how the data was collected. | ✓ |
| image-local-path | string | Local relative or absolute path to a directory in which (also its sub-directories) the referenced image files are located. Absolute paths must start with and relative paths without path separator (ignoring drive letters on windows). The default is the relative path `../raw`. |

In JSON, the `image-set-items` part is one dictionary. The keys in that dictionary are the filenames of the items in 
the image set. Each item, indexed by the `image-filename`, is itself a list ob dictionaries. For videos, the first 
entry of the list contains the default fields for the entire video. Do not repeat static metadata for each second of 
a video to avoid repetition. Every subsequent entry contains specifications that supersede the default values for 
one specific time point of the video. For photos (still images) this list has only one entry, containing the fields 
tabled above.

## UUIDs and hashes
### UUID
Making data FAIR requires - amongst other things - that data is assigned a persistent identifier (PID). Many such 
PID systems exist, and usually they are based on the handle system (like DOIs for example) and alpha-numerical IDs 
that are globally unique. For images, we chose to use UUIDs (*Universally Unique Identifiers*), more precisely UUID 
type 4: random. These UUIDs can be created by anyone and as there are ca 21^36 possible UUID4s making it almost 
impossible that the same one is created more than once. This UUID is the alphanumerical identifier that has to be 
assigned to each image and to each image set. That means that the UUID for an image item (a photo or video) *has to 
become part of the file itself!* You need to write it into the image file's metadata header. How this can be done 
depends on the image file format you chose but in general the two magnificent software tools *exiftool* anf *ffmpeg* 
are the solution. In case you are using the MarIQT software, these tools are used under the hood.

### Hash
We use hashes to document the integrity of the image files. A hash is like a fingerprint of the file as it is 
computed from the file's byte content. If you like, it is a massive compression of the file's data into a short, 
cryptic text (ca. 32 characters) but unlike a zip file there is no way to uncompress the file from the hash. Using 
hashes allows us to make sure that a file has not gone corrupt or that a particular file is actually the version we 
are interested in. Checking the integrity of a file with hashes requires, that the byte content does not change. It 
is therefore absolutely essential, that the UUID is written to the image file's metadata header **before** the hash 
for that file is computed!
