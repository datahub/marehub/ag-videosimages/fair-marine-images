> **Preamble:** We strive to make marine image data [FAIR](FAIR-marine-images.md). We maintain the iFDO [metadata 
> vocabulary](ifdos/iFDO-overview.md) to establish a common language for describing marine imagery, we provide 
> [metadata schemas](https://codebase.helmholtz.cloud/datahub/marehub/ag-videosimages/fair-marine-images/-/tree/v2.0.0/resources/schemas/ifdo-v2.0.0.json) to link the iFDO format to established metadata standards, 
> we develop 
> best-practice [standard operating procedures](sops/sop-overview.md) for handling marine images, and we 
> implement [software tools](https://gitlab.hzdr.de/datahub/marehub/ag-videosimages/) to apply the vocabulary and procedures to marine imagery.

## FAIRness
The buzzterm [FAIR](https://www.go-fair.org/fair-principles/) is an acronym for Findable, Accessible, Interoperable and Reusable. Which you probably knew already. Sorry. But in case you didn't: its the big idea, currently driving research data management (RDM) and a massive challenge for RDM infrastructure providers, maintainers and users. But it is also a fantastic opportunity to open up research by making research efforts more prominent and results more reliable. It appears everywhere nowadays, most importantly in project calls and thus project proposals. So while there is also no way around it, embracing this new data culture will change the face of research for the better!

## FAIR Digital Objects for images (iFDOs)
Achieving FAIRness and Openness of (marine) image data requires structured and standardised metadata on the image data itself and the visual and semantic image data content. This metadata shall be provided in the form of FAIR digital objects (FDOs). These documentation pages describes how FDOs for images (aka iFDOs) shall be structured. If you want, an iFDO is a human and machine-readable file format for an entire image set, except that it does not contain the actual image data, only references to it through persistent identifiers!

## And where are the images?
That is entirely up to you! iFDOs only contain metadata - including information on where images can be found. Ideally image data is also openly accessible, but this is not mandatory. Your images will likely be stored in a media asset management system (MAMS) that is ideally web-accessible and where the persistent identifiers in an iFDO file link to. So its easy to share image metadata data with iFDOs (they are small enough to be emailed if needed) and you simultaneously share the image data without moving it physically.

## How do I get started using iFDOs?
1. Read this documentation to [explore the metadata fields](ifdos/iFDO-overview.md) that are required or constrained
2. Install a software that understands the iFDO concept (e.g. mariqt)
3. Look at [example iFDO](https://codebase.helmholtz.cloud/datahub/marehub/ag-videosimages/fair-marine-images/-/tree/v2.0.0/resources/examples/ifdo-test-v2.0.0.json) files
4. Collect your own image metadata, maybe by using the provided [Excel sheets](https://codebase.helmholtz.cloud/datahub/marehub/ag-videosimages/fair-marine-images/-/tree/v2.0.0/resources/sheets)

## Journal publication
The principles and workflows - described on these pages here - have been published in detail as a peer-reviewed journal 
article in Nature Scientific Data: https://doi.org/10.1038/s41597-022-01491-3 Please cite this article in your own 
works in case you use iFDOs for your data.

## Standard Operating Procedures (SOPs)
More hands-on material on creating and using iFDOs is available in two OceanBestPractices Bundles. One on [iFDO Creation](http://hdl.handle.net/11329/1782) and one on [Image curation and publication](http://hdl.handle.net/11329/1781).	

### Vocabulary terms - what are images?
We use the following wording - based on Dublin Core - throughout these pages: [images](http://purl.org/dc/dcmitype/Image) are both photos (still images) and videos (moving images) acquired by cameras, recording the optical spectrum of light. [Still images](http://purl.org/dc/dcmitype/StillImage) are static visual representations, while [moving images](http://purl.org/dc/dcmitype/MovingImage) are a series of visual representations imparting an impression of motion when shown in succession. An [image set](http://purl.org/dc/dcmitype/Collection) is a collection of at least one, but usually many, images. We further use <tags> as placeholders for specific information (like a variable).

## Imprint
These pages provide information created by several authors. You can contact Timm Schoening (https://orcid.org/0000-0002-0035-3282) for details. With major contributions from:

<img src="https://codebase.helmholtz.cloud/uploads/-/system/group/avatar/8380/HMC_Logo_SocialMedia_drk.jpg" alt="HMC logo" width="150" height="auto"> 
<img src="https://datahub.hcdc.hereon.de/static/images/logo.png" alt="DataHub logo" width="150" height="auto"> 
<img src="https://marine-imaging.com/assets/figures/mi_logo_2021.png" alt="MIC logo" width="150" height="auto">
