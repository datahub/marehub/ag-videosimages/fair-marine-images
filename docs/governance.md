# Governance
Over its first year of existence, the iFDO format was developed by members of the MareHub working group on 
Images/Videos and through the Helmholtz-funded project _FDO-5DI_ of DLR and GEOMAR. Additional input was 
collected from the international Marine Imaging Community at the Marine Imaging Workshops.

Everyone is invited to contribute to the further development of the iFDO format. Please submit an issue in the 
repository, or send an email to ifdo [at] marine-imaging.com to recommend changes.

At present, the Marine Imaging Community is setting up an iFDO steering board of members from academia, industry and 
governance with international to develop the iFDO roadmap from the community inputs. Please write an e-mail to 
ifdo [at] marine-imaging.com in case you are interested in joining this steering board.

### History of iFDO development

**v2.1.0:** Should include a mechanism to represent stereo imagery

**v2.0.0 (current):** Major changes in format and to include missing information provided by the community

**v1.1.0:** Updated fields and added more detailed, consistent and specific documentation.

**v1.0.0:** Initial publication of the iFDO concept through the repository at https://gitlab.hzdr.
de/datahub/marehub/ag-videosimages/fair-marine-images and with static supplementary material published as OceanBest Practice (https://hdl.handle.net/11329/1781 and https://hdl.handle.net/11329/1782).


