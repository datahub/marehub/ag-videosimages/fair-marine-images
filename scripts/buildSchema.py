# This script takes the name of a schemas as an input and builds the human- and machine-readable documentation of
# that schemas into the docs/schemas folder.
import os

import sys
import json
import shutil
from datetime import datetime
import jsonschema
from pprint import pprint


def getSchemaNameAndVersion(filename):
    l = len(filename)
    return filename[0:l-12], filename[l-11:l-5]


def preparePaths(schema_name, schema_version):
    # Prepare empty target folder
    schema_path = "../docs/schemas/" + schema_name + "/" + schema_version + "/"
    if os.path.exists(schema_path):
        shutil.rmtree(schema_path)
    if not os.path.exists("../docs/schemas/" + schema_name):
        os.mkdir("../docs/schemas/" + schema_name)
    if not os.path.exists(schema_path):
        os.mkdir(schema_path)
    return schema_path


def getHeaderInformation(j):
    # Create generic schema header
    header_json = {}
    header_json["$schema"] = j["$schema"]
    header_json["$id"] = j["$id"]
    header_json["title"] = j["title"]
    header_json["description"] = j["description"]
    return header_json


def addPropertiesAsText(properties):
    html = ''
    for tmp in properties:
        html += tmp + ' (' + properties[tmp]["type"]+'): '+properties[tmp]["description"]+'<br>'
    return html


def buildPageForField(path, schema_name, schema_version, field, properties, header, field_context, schema_url):
    print(path, field)

    now = datetime.now()
    datetime_str = now.strftime("%Y-%m-%d %H:%M:%S")

    html =  '<!doctype html>\n<html lang="en">\n<head>\n<meta charset="utf-8">\n' \
            '<title>Term '+field+' from '+schema_name+' schema '+schema_version+'</title>\n' \
            '<style>body {font-family:Arial;} h1{color:#8CB325;} .muted{' \
                                              '} .key{' \
                                              'font-weight:bold;padding: 5px;' \
                                              'text-align:right;min-width:250px;vertical-align:top;} .val {' \
                                              'padding:5px;}.content{max-width: 800px;margin:auto;}</style>' \
            '<meta name="title" content="Term '+field+' from '+schema_name+' schema '+schema_version+'">' \
            '<meta name="date" content="' + datetime_str + '">' \
            '<meta name="description" content="A metadata field description for '+schema_name+'' \
            ' '+schema_version+' field '+field+'">' \
            '<link rel="schema.DC" href="http://purl.org/dc/elements/1.1/">' \
            '<link rel="schema.DCTERMS" href="http://purl.org/dc/terms/">' \
            '<meta name="DC.title" content="Term ' + field + ' from ' + schema_name + ' schema ' + schema_version + \
            '">' \
            '<meta name="DC.date" content="' + datetime_str + '">' \
            '<meta name="DC.description" content="A metadata field description for ' + schema_name + '' \
            ' ' + schema_version + ' field ' + field + '">' \
            '<meta name="DCTERMS.license" scheme="DCTERMS.URI" ' \
            'content="https://creativecommons.org/licenses/by-nc/3.0/">' \
            '<meta name="DC.identifier" content="' + schema_url+field + '" scheme="DCTERMS.URI">'

    # Add JSON-LD block:
    html += '<script type="application/ld+json"> {' \
            '"@context": "http://schema.org/",' \
            '"@id": "' + schema_url+field + '",' \
            '"@type": "DefinedTerm",' \
            '"identifier": "' + schema_url+field + '",' \
            '"url": "' + schema_url+field+ '",' \
            '"name": Term ' + field + ' from ' + schema_name + ' schema ' + schema_version + '",' \
            '"datePublished": ' + datetime_str + '",' \
            '"description": "A metadata field description for '+schema_name+'' \
            ' '+schema_version+' field '+field+'",' \
            '"license": "https://creativecommons.org/licenses/by-nc/3.0/",' \
            '"conditionsOfAccess": "unrestricted", "isAccessibleForFree": true'

    # TODO Eventually add @context information about other metadata standards that use this field?!

    html += '} </script>'

    html += '\n</head>\n<body><div class="content">\n'
    html += '<h1>'+field+'</h1>\n'
    html += '<p class="muted">This field is part of the schema "' + header["title"] + '": ' + header["description"] + \
            '</p>\n'
    html += '<table>'
    html += '<tr><td class="key">schema</td><td class="val"><a href="'+header["$id"]+'">'+header["$id"]+'</a></td></tr>\n'

    for tmp in properties:
        html += '<tr><td colspan="2"><hr style="border:0.1px solid #A9C65C;"></td></tr>'
        if tmp == "properties":
            html += '<tr><td class="key">' + tmp + '</td><td class="val">' + addPropertiesAsText(properties[tmp]) + '</td></tr>\n'
        elif tmp == "$ref":
            html += '<tr><td class="key">' + tmp + '</td><td class="val"><a href="' + str(properties[tmp]) + '">' \
                    +str(properties[tmp]) +'</a></td></tr>\n'
        else:
            html += '<tr><td class="key">' + tmp + '</td><td class="val">' + str(properties[tmp]) + '</td></tr>\n'

    # Eventually add @context information about other metadata standards that use this field
    for ctx in field_context:
        html += '<tr><td colspan="2"><hr style="border:0.1px solid #A9C65C;"></td></tr>'
        html += '<tr><td class="key">' + ctx["term"] + '</td><td class="val">(equivalent term from schema: ' + ctx[
            "schema"] + ')</td></tr>'
    html += '</div></body>\n</html>\n'

    with open(path, "w") as out:
        out.write(html)


def buildOverviewIndexPageForSchema(path, schema_name, schema_version, fields, header, schema_url):
    print("Building schema overview page", path, schema_name, schema_version)

    now = datetime.now()
    datetime_str = now.strftime("%Y-%m-%d %H:%M:%S")

    html =  '<!doctype html>\n<html lang="en">\n<head>\n<meta charset="utf-8">\n' \
            '<title>Schema '+schema_name+' ('+schema_version+')</title>\n' \
            '<style>body {font-family:Arial;} h1{color:#8CB325;} .muted{' \
                                              '} .key{' \
                                              'font-weight:bold;padding: 5px;' \
                                              'text-align:right;min-width:250px;vertical-align:top;} .val {' \
                                              'padding:5px;}.content{max-width: 800px;margin:auto;}</style>' \
            '<meta name="title" content="Schema '+schema_name+' ('+schema_version+')">' \
            '<meta name="date" content="' + datetime_str + '">' \
            '<meta name="description" content="Introduction to the metadata schema '+schema_name+'' \
            ' ('+schema_version+')">' \
            '<link rel="schema.DC" href="http://purl.org/dc/elements/1.1/">' \
            '<link rel="schema.DCTERMS" href="http://purl.org/dc/terms/">' \
            '<meta name="DC.title" content="Schema '+schema_name+' ('+schema_version+')">' \
            '<meta name="DC.date" content="' + datetime_str + '">' \
            '<meta name="DC.description" content="Introduction to the metadata schema '+schema_name+'' \
            ' ('+schema_version+')">' \
            '<meta name="DCTERMS.license" scheme="DCTERMS.URI" ' \
            'content="https://creativecommons.org/licenses/by-nc/3.0/">' \
            '<meta name="DC.identifier" content="' + schema_url+ '" scheme="DCTERMS.URI">'

    # Add JSON-LD block:
    html += '<script type="application/ld+json"> {' \
            '"@context": "http://schema.org/",' \
            '"@id": "' + schema_url + '",' \
            '"@type": "DefinedTerm",' \
            '"identifier": "' + schema_url + '",' \
            '"url": "' + schema_url+ '",' \
            '"name": Schema '+schema_name+' ('+schema_version+')",' \
            '"datePublished": ' + datetime_str + '",' \
            '"description": "Introduction to the metadata schema '+schema_name+'' \
            ' ('+schema_version+')",' \
            '"license": "https://creativecommons.org/licenses/by-nc/3.0/",' \
            '"conditionsOfAccess": "unrestricted", "isAccessibleForFree": true'

    html += '} </script>'

    html += '\n</head>\n<body><div class="content">\n'
    html += '<h1>'+schema_name+' metadata schema</h1>\n'
    html += '<a href="' + header["$id"] + '">' + header["$id"] + '</a><br>\n'
    html += '<p class="muted">This is an overview of metadata fields of the schema "' + header["title"] + \
            '":<br>' + header["description"] + \
            '</p>\n'
    html += '<table>'

    for tmp in fields:
        html += '<tr><td colspan="2"><hr style="border:0.1px solid #A9C65C;"></td></tr>'
        if "description" in fields[tmp]:
            html += '<tr><td class="key"><a href="'+tmp+'">' + tmp + '</a></td><td class="val">' + fields[tmp][
                'description'] + '</td></tr>\n'
        elif "$ref" in fields[tmp]:
            html += '<tr><td class="key">' + tmp + '</td><td class="val"><a href="' + fields[tmp]["$ref"] + '">' \
                    + fields[tmp]["$ref"] + '</a></td></tr>\n'
        else:
            print("SKIPPING",tmp)
    html += '</div></body>\n</html>\n'

    with open(path,"w") as out:
        out.write(html)


def buildSchema(path, schema_name, schema_version, header, j, context, schema_url):

    for tmp in j:

        print(tmp)
        tmp_path = path + tmp

        if "$ref" in j[tmp]:
            # This is a linked field to another schema
            print("TODO:",tmp,"(linked)")

        if tmp in context:
            field_context = context[tmp]
        else:
            field_context = []

        # Build human-readable page
        buildPageForField(tmp_path, schema_name, schema_version, tmp, j[tmp], header, field_context, schema_url)

        # Build machine-readable json -> NO! The JSON is now contained within the html!! The only JSON schema is the
        # singular file.

    # Build one overview index file
    buildOverviewIndexPageForSchema(path+"index.html", schema_name, schema_version, j, header, schema_url)


# TODO RefResolver is deprecated in jsonschema since version 4.18
def resolve_jsonschema(obj:dict,schema:dict,store:dict=None,resolver:jsonschema.RefResolver=None,only_intern=False):
	""" Return schema with all $refs resolved """
	if resolver is None:
		if store is None:
			resolver = jsonschema.RefResolver.from_schema(schema)
		else:
			resolver = jsonschema.RefResolver.from_schema(schema,store=store)
	
	if isinstance(obj,dict):
		ret = {}
		resolved = {}
		for key, val in obj.items():
			if key == "$ref":
				# local
				resolved_ret = resolver.resolve(val)
				# internal ref starts with #
				if '#' in val and val.split('#')[0] == "":
					resolved = resolved_ret[1]
					resolved = resolve_jsonschema(resolved,schema,store,resolver,only_intern)
				# external ref
				else:
					if not only_intern:
						url = resolved_ret[0]
						# resolve complete external schema first
						base_schema = resolver.resolve(url.split('#')[0])[1]
						resolved_base_schema = resolve_jsonschema(base_schema,base_schema,only_intern=only_intern)
						sub_resolver = jsonschema.RefResolver.from_schema(resolved_base_schema)
						# resolve val on completely resolved external schema
						resolved = sub_resolver.resolve(val)[1]
					else:
						ret[key] = val
			else:
				ret[key] = resolve_jsonschema(val,schema,store,resolver,only_intern)
		ret.update(resolved)
		return ret
	
	if isinstance(obj,list):
		ret = []
		for item in obj:
			ret.append(resolve_jsonschema(item,schema,store,resolver,only_intern))
		return ret

	# otherwise (string)
	return obj


if len(sys.argv) != 2:
    print("Usage: buildSchemas.py <schemas-file>")
else:
    if not os.path.exists("../docs/schemas/"+sys.argv[1]):
        print("Schema", sys.argv[1], "not found")
    else:
        with open("../docs/schemas/"+sys.argv[1]) as json_data:

            j = json.load(json_data)
            json_data.close()

            # # load all schema and add to store
            # schemas = [file for file in os.listdir("../docs/schemas/") if file.split('.')[-1] == 'json']
            # schema_store = {}
            # for schema_file in schemas:
            #     with open("../docs/schemas/" + schema_file) as f:
            #         schema = json.load(f)
            #     schema_store[schema['$id']] = schema
            # # resolve all refs in schema
            # j = resolve_jsonschema(j, j, schema_store)
            # resolve intern refs
            j = resolve_jsonschema(j, j,only_intern=True)
            #pprint(j)

            # Get schema name and version from filename
            schema_name, schema_version = getSchemaNameAndVersion(sys.argv[1])

            # Get name and version of schemas
            schema_path = preparePaths(schema_name, schema_version)

            # Fetch header information from json
            header_json = getHeaderInformation(j)

            schema_url = header_json["$id"].replace(sys.argv[1], "") + schema_name + "/" + schema_version + "/"
            print(schema_url)

            # Check whether there is an @context section available to map to other metadata standards
            context = {}
            if "@context" in j:
                for ctx in j["@context"]:
                    for key in ctx:
                        if not "image-" in key:
                            ctx_uri = ctx[key]
                        else:
                            if key not in context:
                                context[key] = []
                            context[key].append({"schema":ctx_uri,"term":ctx[key]})

            if schema_name == "ifdo":
                print("Creating ifdo schema")
                buildSchema(schema_path, schema_name, schema_version, header_json, j["$defs"]["iFDO-fields"]["properties"]
                    , context, schema_url)
            else:
                print("Creating other schema")
                buildSchema(schema_path, schema_name, schema_version, header_json, j["properties"],context, schema_url)
