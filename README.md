> **Preamble:** We strive to make marine image data [FAIR](docs/FAIR-marine-images.md). We maintain the iFDO [metadata 
> vocabulary](docs/ifdos/iFDO-overview.md) to establish a common language for describing marine imagery, we provide 
> [metadata schemas](resources/schemas/ifdo-v2.0.0.json) to link the iFDO format to established metadata 
> standards, we develop 
> best-practice [standard operating procedures](docs/sops/sop-overview.md) for handling marine images, and we 
> implement [software tools](https://gitlab.hzdr.de/datahub/marehub/ag-videosimages/) to apply the vocabulary and procedures to marine imagery.

## How do I get started using iFDOs?
1. Read this documentation to [explore the metadata fields](docs/ifdos/iFDO-overview.md) that are required or constrained
2. Install a software that understands the iFDO concept (e.g. mariqt)
3. Look at [example iFDO](resources/examples/ifdo-test-v2.0.0.json) files
4. Collect your own image metadata, maybe by using the provided [Excel sheets](resources/sheets)

## Where are the details?
The principles and workflows - described on these pages here - have been published in detail as a peer-reviewed 
journal article in Nature Scientific Data: https://doi.org/10.1038/s41597-022-01491-3
Please cite this article in your own works in case you use iFDOs for your data.

